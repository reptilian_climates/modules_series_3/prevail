
'''
	/* list-style-type: circle; */
'''


def build (
	main = "",
	script = ""
):

	styles = """
body {
	margin: 0;
	padding: 0;
}

h1, h2, h3, p, ul, li {
	margin: 0;
	padding: 0;
}

h1, h2, h3 {
	font-weight: normal;
	font-style: italic;
}

p {
	font-weight: normal;
	font-style: normal;
}

li {
	
	list-style-type: disclosure-closed
}

ul {
	padding-left: 20px;
}

main {
	position: relative;
	margin: 0 auto;
	width: 8.5in;
	height: 11in;
}		

div[articles-start] {
	position: relative;
	margin: 0 auto;
	width: 8.5in;
	visibility: hidden;
}

	"""
	


	return (f"""
<!DOCTYPE html>
<html>
<head></head>
<body>
<style>
	{ styles }
</style>
<div articles-start>
	{ main }
</div>
<main articles>


</main>
<script>
	{ script }
</script>
</body>
	""")
