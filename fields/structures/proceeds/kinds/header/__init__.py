
'''
import proceeds.kinds.header as header
header.build (structure)
'''
from mako.template import Template
import proceeds.modules.paragraph as paragraph

import proceeds.climate as climate
import proceeds.modules.panel as panel
	

def build (structure):
	name = structure ["name"]
	summary = structure ["summary"]

	if ("background" in structure):
		background = structure ["background"]
	else:
		background = "none"

	p1 = paragraph.build (name)
	p2 = paragraph.build (summary)
	
	border_width = climate.find () ["layout"] ["border width"]
	
	mako_template = Template (
f"""
<section
	kind-header
	tile
	style="
		position: relative;
		overflow: hidden;
	
		border: { border_width } solid black;
		border-radius: .1in;
		padding: .25in;
	
		border-bottom: .1in solid none;
	
		display: flex;
		justify-content: space-between;
		align-items: center;
	"
>
	<img 
		style="
			position: absolute;
			top: 0;
			left: 0;
			
			width: 100%;
			
			opacity: .3;
		"
	
		src="{ background }" 
	/>
		
	<div>
		<label>name</label>
		{ p1 }
	</div>
	
	<div>
		<label>summary</label>
		{ p2 }
	</div>
</section>
""")

	return panel.build (
		mako_template.render (name = name)
	)

