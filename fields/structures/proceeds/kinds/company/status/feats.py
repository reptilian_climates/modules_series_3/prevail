



import proceeds.climate as climate
border_width = climate.find () ["layout"] ["border width"]
	
	
def introduce (FEATS):
	START = ("""
	<section
		style="
			display: flex;
		"
	>
		<h3
			style="
				width: 100px;
			"
		>feats</h3>
		<ul>
""")
	
	END = ("""
		</ul>
	</section>
""")

	STRING = ""
	
	for FEAT in FEATS:
		STRING += (
f"""			<li>{ FEAT }</li>"""
		)

	return START + STRING + END;