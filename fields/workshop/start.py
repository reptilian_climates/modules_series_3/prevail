
'''
	python3 start.py
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])

import pathlib
from os.path import dirname, join, normpath
import sys
this_folder = pathlib.Path (__file__).parent.resolve ()	
picture = normpath (join (this_folder, "Avila.png"))

import proceeds
import proceeds.photo.build as photo_builder


proceeds.abundantly ({
	"build": [{
		"kind": "header",
		"fields": {
			#"name": "superior elite alpha person",			
			"name": "Great Superior Person",
			
			"background": photo_builder.start (picture),
			"summary": "\n".join ([
				"To seek paragon renown in the field of technology is an ambition worthy of dedication."
			])
		}
	},{
		"kind": "company",
		"fields": {
			"name": "A company named company",
			"descriptions": [],
			"statuses": [{
				"names": [ "paragon of creation" ],
				"places": [ "Mostly the multiverse" ],
				"dates": [ "3041 January 13", "3098 March 4" ],
				"feats": [
					"Made $903424782934897234",
					"Cured every disease",
					"Created infinity*E(infinity)(i) new multiverses"
				]
			},{
				"names": [ "untitled" ],
				"places": [ "wherever" ],
				"dates": [ "not entirely sure" ],
				"feats": [ "mostly just relaxed" ]
			}]
		}
	},{
		"kind": "company 2",
		"fields": {
			"name": "company 4",
			"descriptions": [],
			"statuses": [{
				"names": [ ],
				"places": [ ],
				"dates": [ ],
				"feats": []
			}]
		}
	},{
		"kind": "project",
		"fields": {
			"name": "project 1",
			"summary": [
				"This is a description of the project,",
				"this is the second line."
			]
		}
	},{
		"kind": "project",
		"fields": {
			"name": "project 1",
			"summary": [
				"This is a description of the project,",
				"this is the second line."
			]
		}
	},{
		"kind": "project",
		"fields": {
			"name": "project 1",
			"summary": [
				"This is a description of the project,",
				"this is the second line."
			]
		}
	},{
		"kind": "project",
		"fields": {
			"name": "project 1",
			"summary": [
				"This is a description of the project,",
				"this is the second line."
			]
		}
	},{
		"kind": "project",
		"fields": {
			"name": "project 5",
			"summary": [
				"This is a description of the project,",
				"this is the second line."
			]
		}
	},{
		"kind": "academics",
		"fields": {
			"name": "academics 1",
		}
	}]
})